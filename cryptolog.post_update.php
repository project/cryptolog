<?php

/**
 * @file
 * Post update functions for Cryptolog.
 */

/**
 * Migrate Cryptolog service from event subscriber to stack middleware.
 */
function cryptolog_post_update_stack_middleware(): void {
}

/**
 * Update service arguments for Cryptolog middleware.
 */
function cryptolog_post_update_service_arguments(): void {
}

/**
 * Initialize integer type of new TTL configuration.
 */
function cryptolog_post_update_ttl_config(): void {
}
