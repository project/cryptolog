<?php

namespace Drupal\Tests\cryptolog\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests Cryptolog module.
 *
 * @group cryptolog
 */
class CryptologReverseProxyTest extends BrowserTestBase {

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['cryptolog', 'cryptolog_test', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createContentType(['type' => 'page']);
  }

  /**
   * Send requests with Forwarded header and check base URL generation.
   *
   * @return array{array{array<string, string>, string}}
   *   Data for testNodeCanonicalUrlScheme().
   */
  public static function providerTestNodeCanonicalUrlScheme(): array {
    return [
      [['Forwarded' => 'for=192.0.2.0;proto=http'], 'base_insecure_url'],
      [['Forwarded' => 'for=192.0.2.0;proto=https'], 'base_secure_url'],
    ];
  }

  /**
   * Tests that Cryptolog preserves correct URL scheme.
   *
   * @param array<string, string> $headers
   *   An array of headers.
   * @param string $base_url
   *   Which base URL to use.
   *
   * @dataProvider providerTestNodeCanonicalUrlScheme
   */
  public function testNodeCanonicalUrlScheme(array $headers, string $base_url): void {
    $node = $this->createNode();
    $this->drupalGet($node->toUrl(), [], $headers);
    $element = $this->assertSession()->elementExists('css', 'link[rel="canonical"]');
    $this->assertEquals($GLOBALS[$base_url] . '/node/' . $node->id(), $element->getAttribute('href'));
  }

}
