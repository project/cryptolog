<?php

namespace Drupal\Tests\cryptolog\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Cryptolog module.
 *
 * @group cryptolog
 */
class CryptologTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['dblog', 'cryptolog'];

  /**
   * Tests that cryptolog rewrites the client IP address.
   */
  public function testCryptolog(): void {
    // Create user.
    $admin_user = $this->drupalCreateUser(['access site reports']);
    $this->assertInstanceOf(AccountInterface::class, $admin_user);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/reports/dblog/event/1');
    $hostname_1 = $this->getLoggedHostname();
    $this->drupalGet('admin/reports/dblog/event/2');
    $hostname_2 = $this->getLoggedHostname();
    $this->assertNotEquals($hostname_1, $hostname_2);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/reports/dblog/event/3');
    $hostname_3 = $this->getLoggedHostname();
    $this->assertSame($hostname_2, $hostname_3);
  }

  /**
   * Tests the "single web node" configuration.
   */
  public function testConfiguration(): void {
    // Repeat test with single web node enabled.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->assertInstanceOf(AccountInterface::class, $admin_user);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/people/cryptolog');
    $this->submitForm(['single_webhead' => TRUE], 'Save configuration');
    $this->testCryptolog();

    // Test the TTL config.
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/people/cryptolog');
    $this->submitForm(['ttl' => 0], 'Save configuration');
    $this->assertSession()->pageTextContains('TTL (seconds) must be higher than or equal to 21600.');
    $this->submitForm(['invalidate' => TRUE, 'ttl' => 21600], 'Save configuration');
    $admin_user = $this->drupalCreateUser(['access site reports']);
    $this->assertInstanceOf(AccountInterface::class, $admin_user);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/reports/dblog/event/12');
    $hostname_1 = $this->getLoggedHostname();
    $this->drupalGet('admin/reports/dblog/event/15');
    $hostname_2 = $this->getLoggedHostname();
    $this->assertNotEquals($hostname_1, $hostname_2);
  }

  /**
   * Gets the logged hostname from the dblog details page.
   */
  public function getLoggedHostname(): ?string {
    $rows = $this->xpath('//table[@class="dblog-event"]/tbody/tr');
    foreach ($rows as $row) {
      if ($header = $row->find('xpath', '/th')) {
        if ($header->getText() == 'Hostname') {
          if ($cell = $row->find('xpath', '/td')) {
            return $cell->getText();
          }
        }
      }
    }
    return NULL;
  }

}
