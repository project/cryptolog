<?php

namespace Drupal\cryptolog_test;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides support for reverse proxies.
 */
class TestProxyMiddleware implements HttpKernelInterface {

  /**
   * Constructs a ReverseProxyMiddleware object.
   */
  public function __construct(
    protected HttpKernelInterface $httpKernel,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = TRUE): Response {
    // Add the test runner IP address as trusted proxy.
    $proxies = $request->getTrustedProxies();
    $proxies[] = 'REMOTE_ADDR';
    $request::setTrustedProxies($proxies, Request::HEADER_FORWARDED);
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
