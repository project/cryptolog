<?php

namespace Drupal\cryptolog;

use Drupal\Core\Cache\ApcuBackendFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Cryptolog middleware.
 */
class CryptologMiddleware implements CryptologMiddlewareInterface, HttpKernelInterface {

  /**
   * The original client IP address.
   */
  protected ?string $clientIp;

  /**
   * The HTTP host if it was set by Cryptolog.
   */
  protected string $httpHost;

  /**
   * Whether or not this request is from a trusted reverse proxy.
   */
  protected bool $isFromTrustedProxy;

  /**
   * Whether or not the request is secure, if set by Cryptolog.
   */
  protected bool $isSecure;

  /**
   * Constructs the Cryptolog middleware.
   */
  public function __construct(
    protected HttpKernelInterface $httpKernel,
    protected CacheBackendInterface $cacheBackend,
    ?ApcuBackendFactory $apcuBackendFactory = NULL,
    protected ?int $ttl = NULL,
  ) {
    // Allow single-web-node sites to avoid database storage by injecting the
    // APCu backend.
    if ($apcuBackendFactory && function_exists('apcu_enabled') && apcu_enabled()) {
      $this->cacheBackend = $apcuBackendFactory->get('bootstrap');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheBackend(): CacheBackendInterface {
    return $this->cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientIp(): ?string {
    return $this->clientIp ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHttpHost(): ?string {
    return $this->httpHost ?? NULL;
  }

  /**
   * Returns the TTL.
   */
  public function getTtl(): int {
    return $this->ttl ?? static::TTL;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = TRUE): Response {
    if ($type === static::MAIN_REQUEST) {
      $this->setClientIp($request);
    }
    return $this->httpKernel->handle($request, $type, $catch);
  }

  /**
   * {@inheritdoc}
   */
  public function isSecure(): ?bool {
    return $this->isSecure ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isFromTrustedProxy(): ?bool {
    return $this->isFromTrustedProxy ?? NULL;
  }

  /**
   * Sets the client IP address.
   */
  protected function setClientIp(Request $request): void {
    // Once the client IP address is modified, the request will no longer be
    // recognized as coming from a trusted reverse proxy, and therefore the
    // scheme and HTTP host can no longer be dynamically extracted from the HTTP
    // headers (if that's where they were coming from). Save the scheme and HTTP
    // host so they can be restored to the request if necessary.
    if ($this->isFromTrustedProxy = $request->isFromTrustedProxy()) {
      $isSecure = $request->isSecure();
      $httpHost = $request->getHttpHost();
    }

    $this->clientIp = $request->getClientIp();

    if ($cache = $this->cacheBackend->get(static::KEY)) {
      // Support previous versions of Cryptolog.
      $salt = is_array($cache->data) ? $cache->data['salt'] : $cache->data;
    }
    else {
      $salt = random_bytes(32);
      $this->cacheBackend->set(static::KEY, $salt, $this->getTtl() + $request->server->get('REQUEST_TIME'));
    }

    // Client IP should typically exist, but if not we're done.
    if (!isset($this->clientIp)) {
      return;
    }

    // Generate a keyed hash using sodium extension, if available.
    $hmac = function_exists('sodium_crypto_generichash') ? sodium_crypto_generichash($this->clientIp, $salt, 16) : hash_hmac('md5', $this->clientIp, $salt, TRUE);
    // Convert to IPv6 address notation.
    $request->server->set('REMOTE_ADDR', inet_ntop($hmac));

    // For reverse proxy requests, restore the scheme and HTTP host if they do
    // not match their previous values. Record the changes we're making for
    // diagnostic purposes.
    if ($this->isFromTrustedProxy) {
      if ($request->isSecure() !== $isSecure) {
        $request->server->set('HTTPS', $isSecure ? 'on' : 'off');
        $this->isSecure = $isSecure;
      }
      if ($request->getHttpHost() !== $httpHost) {
        $request->headers->set('HOST', $httpHost);
        $this->httpHost = $httpHost;
      }
    }
  }

}
