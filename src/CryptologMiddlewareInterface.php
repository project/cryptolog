<?php

namespace Drupal\cryptolog;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Defines the Cryptolog middleware interface.
 */
interface CryptologMiddlewareInterface {

  /**
   * The cache identifier.
   */
  const KEY = 'cryptolog';

  /**
   * The default cache time to live in seconds.
   */
  const TTL = 86400;

  /**
   * Returns the backend cache bin for diagnostic purposes.
   */
  public function getCacheBackend(): CacheBackendInterface;

  /**
   * Returns the original client IP address for diagnostic purposes.
   */
  public function getClientIp(): ?string;

  /**
   * Returns the HTTP host if it was set by Cryptolog.
   */
  public function getHttpHost(): ?string;

  /**
   * Returns whether or not the request is secure, if set by Cryptolog.
   */
  public function isSecure(): ?bool;

  /**
   * Returns whether or not the request is from a trusted reverse proxy.
   */
  public function isFromTrustedProxy(): ?bool;

}
