<?php

namespace Drupal\cryptolog;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines Cryptolog settings form.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected CryptologMiddlewareInterface $cryptologMiddleware,
    protected DateFormatterInterface $dateFormatter,
    protected TimeInterface $time,
    protected Settings $settings,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('cryptolog.middleware'),
      $container->get('date.formatter'),
      $container->get('datetime.time'),
      $container->get('settings'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cryptolog_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   The editable config names.
   */
  protected function getEditableConfigNames() {
    return [
      'cryptolog.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The settings form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cryptolog.settings');
    $form['salt'] = [
      '#type' => 'details',
      '#title' => $this->t('Salt storage and time to live (TTL)'),
      '#description' => $this->t('Cryptolog uses a salt to map each client IP address to an ephemeral identifier (a 128-bit keyed hash in IPv6 notation). By rotating the salt, Cryptolog ensures that the original client IP addresses cannot be determined (e.g. by means of a rainbow table). Use of a memory-backed cache (such as APCu, Memcache or Redis) is recommended to avoid storing the salt on disk or backing it up.'),
      '#open' => TRUE,
    ];
    $form['salt']['backend'] = [
      '#type' => 'item',
      '#title' => $this->t('Storage backend'),
      '#plain_text' => get_class($this->cryptologMiddleware->getCacheBackend()),
      '#description' => $this->t('The cache backend in which the salt is stored until it expires.'),
    ];
    $form['salt']['single_webhead'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This site has a single web node; store salt solely in APCu cache'),
      '#default_value' => $config->get('single_webhead'),
      '#description' => $this->t("If enabled and APCu is available, Cryptolog will store the salt solely in the APCu cache. Otherwise, Cryptolog will share the salt between multiple web nodes, storing it in the database unless <code>\$settings['cache']['default']</code> is configured to use Memcache, Redis, etc."),
    ];
    $salt = $this->cryptologMiddleware->getCacheBackend()->get($this->cryptologMiddleware::KEY);
    $form['salt']['expiry'] = [
      '#type' => 'item',
      '#title' => $this->t('Time remaining'),
      '#markup' => $this->dateFormatter->formatInterval(isset($salt->expire) ? ($salt->expire - $this->time->getRequestTime()) : 0),
      '#description' => $this->t('The time remaining until the salt expires and is automatically regenerated.'),
    ];
    $floodConfig = $this->config('user.flood');
    $form['salt']['ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('TTL (seconds)'),
      '#default_value' => $config->get('ttl'),
      '#min' => $floodConfig->get('uid_only') ? $floodConfig->get('ip_window') : $floodConfig->get('user_window'),
      '#description' => $this->t('When the TTL expires, the salt will be regenerated and each client IP address will map to a new ephemeral identifier. The default is %ttl seconds (@formatted).', [
        '%ttl' => $this->cryptologMiddleware::TTL,
        '@formatted' => $this->dateFormatter->formatInterval($this->cryptologMiddleware::TTL),
      ]),
    ];
    $form['salt']['invalidate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Regenerate salt now'),
      '#default_value' => FALSE,
      '#description' => $this->t('If checked, the salt will be regenerated and each client IP address will map to a new ephemeral identifier.'),
    ];
    $form['diagnostic'] = [
      '#type' => 'details',
      '#title' => $this->t('Diagnostic information'),
      '#open' => TRUE,
    ];
    $form['diagnostic']['cryptolog_ip'] = [
      '#type' => 'item',
      '#title' => $this->t('Cryptolog IP address'),
      '#plain_text' => $this->getRequest()->getClientIp(),
      '#description' => $this->t('Your ephemeral identifier generated by Cryptolog.'),
    ];
    $form['diagnostic']['original_ip'] = [
      '#type' => 'item',
      '#title' => $this->t('Original IP address'),
      '#plain_text' => $this->cryptologMiddleware->getClientIp(),
      '#description' => $this->t('Your client IP address prior to being replaced by Cryptolog. If this is not correct, check your reverse proxy settings in <code>settings.php</code>.'),
    ];
    $form['diagnostic']['trusted_reverse_proxy'] = [
      '#type' => 'item',
      '#title' => $this->t('Trusted reverse proxy'),
    ];
    if (!$this->settings->get('reverse_proxy', FALSE)) {
      $form['diagnostic']['trusted_reverse_proxy']['#markup'] = $this->t('Disabled');
      $form['diagnostic']['trusted_reverse_proxy']['#description'] = $this->t('Reverse proxy settings are disabled.');
    }
    elseif ($this->cryptologMiddleware->isFromTrustedProxy()) {
      $form['diagnostic']['trusted_reverse_proxy']['#markup'] = $this->t('Yes');
      $form['diagnostic']['trusted_reverse_proxy']['#description'] = $this->t('Your reverse proxy settings indicate that this request is from a trusted proxy.');
    }
    else {
      $form['diagnostic']['trusted_reverse_proxy']['#markup'] = $this->t('No');
      $form['diagnostic']['trusted_reverse_proxy']['#description'] = $this->t('This request did not meet trusted proxy criteria.');
    }
    if (NULL !== $this->cryptologMiddleware->getHttpHost()) {
      $form['diagnostic']['http_host'] = [
        '#type' => 'item',
        '#title' => $this->t('HTTP host'),
        '#plain_text' => $this->cryptologMiddleware->getHttpHost(),
        '#description' => $this->t('Cryptolog set the HTTP host while initializing.'),
      ];
    }
    if (NULL !== $this->cryptologMiddleware->isSecure()) {
      $form['diagnostic']['is_secure'] = [
        '#type' => 'item',
        '#title' => $this->t('Protocol'),
        '#markup' => $this->cryptologMiddleware->isSecure() ? $this->t('HTTPS') : $this->t('HTTP'),
        '#description' => $this->t('Cryptolog set the protocol while initializing.'),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('cryptolog.settings')
      ->set('single_webhead', $form_state->getValue('single_webhead'))
      ->set('ttl', $form_state->getValue('ttl'))
      ->save();
    if ($form_state->getValue('invalidate')) {
      $this->cryptologMiddleware->getCacheBackend()->invalidate($this->cryptologMiddleware::KEY);
    }
    parent::submitForm($form, $form_state);
  }

}
