<?php

namespace Drupal\cryptolog;

use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Injects the APCu cache so single-web-node sites can avoid database storage.
 */
class CryptologServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    global $config;
    if (!$container->hasDefinition('cryptolog.middleware')) {
      return;
    }
    $mergedConfig = $config['cryptolog.settings'] ?? [];
    $mergedConfig += BootstrapConfigStorageFactory::get()->read('cryptolog.settings') ?: [];
    $container->getDefinition('cryptolog.middleware')->addArgument(!empty($mergedConfig['single_webhead']) ? new Reference('cache.backend.apcu') : NULL);
    if (isset($mergedConfig['ttl']) && is_int($mergedConfig['ttl'])) {
      $container->getDefinition('cryptolog.middleware')->addArgument($mergedConfig['ttl']);
    }
  }

}
