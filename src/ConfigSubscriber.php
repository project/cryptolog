<?php

namespace Drupal\cryptolog;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\DrupalKernelInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listens to the config save event for cryptolog settings.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the ConfigSubscriber.
   */
  public function __construct(
    protected DrupalKernelInterface $kernel,
  ) {
  }

  /**
   * Invalidates container on config change.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The ConfigCrudEvent to process.
   */
  public function onSave(ConfigCrudEvent $event): void {
    if ($event->getConfig()->getName() === 'cryptolog.settings' && ($event->isChanged('single_webhead') || $event->isChanged('ttl'))) {
      $this->kernel->invalidateContainer();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onSave'];
    return $events;
  }

}
