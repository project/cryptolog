# Cryptolog

Cryptolog enhances user privacy by logging ephemeral identifiers in place of
client IP addresses in Drupal logs and database tables.

After installing Cryptolog, `\Drupal::request()->getClientIp()` outputs a keyed
hash of the client IP address, using a salt that is stored in the cache and
regenerated each day. The keyed hash is rendered in IPv6 notation for
compatibility with any consumers of the `getClientIp()` method.

Because Cryptolog uses the same unique identifier per IP address for a 24-hour
period, site administrators can do statistical analysis of logs such as counting
unique IP addresses per day. In addition, Drupal's flood control mechanism can
function normally.

Note: As long as the salt can still be retrieved, brute force can be used to
generate a rainbow table and reverse engineer client IPv4 addresses. However,
once the salt has expired and a new salt regenerated, or the salt was stored
only in memory and the web server has been shutdown or restarted, it should not
be feasible to determine client IP addresses.


## Installation

Cryptolog requires that PHP was compiled with IPv6 support enabled.

Cryptolog will use the Sodium PHP extension, if available, to generate keyed
hashes, otherwise falling back to the Hash PHP extension.

For best performance, it is recommended to enable APCu PHP extension.


## Configuration

If your site has a single web node and you want to avoid storing the salt on
disk, you should ensure the APCu PHP extension is enabled, visit the Cryptolog
settings page at admin/config/people/cryptolog and enable the "single web node"
setting.

If your site has multiple web nodes, you should disable the "single web node"
setting so the salt can be shared between web nodes. To avoid storing the
salt on disk, install a module which provides an in-memory cache (such as
[Memcache](https://www.drupal.org/project/memcache) or
[Redis](https://www.drupal.org/project/redis)) and set the appropriate default
cache backend in your settings.php file, e.g.:

```php
// If you have installed Memcache module:
$settings['cache']['default'] = 'cache.backend.memcache';
// Or, if you have installed Redis module:
$settings['cache']['default'] = 'cache.backend.redis';
```

The time to live (TTL) of the Cryptolog salt can also be configured on the
settings page. When the TTL expires, the salt will be regenerated and Cryptolog
will map each client IP address to a new ephemeral identifier.

To ensure that the flood control mechanism can function effectively, the salt
TTL should be larger than the window of any flood control events that use IP
address as part of the identifier, which is 21600 seconds (six hours) on a
clean Drupal install. The default salt TTL is 86400 seconds (24 hours).


## Reverse proxy and forwarded header support

Cryptolog module could have side effects for sites that use a reverse proxy and
rely on trusted HTTP headers to determine the request protocol and/or HTTP host.
Because the client IP address no longer matches a trusted reverse proxy, the
HttpFoundation component will stop dynamically extracting request info from the
headers. Cryptolog attempts to detect if the protocol or HTTP host changes after
it initializes, and restores the original values to the environment in a way
that should typically fix things. Check the diagnostic information on the
Cryptolog settings page for more information.


## Bug reports, feature and support requests

See the [Cryptolog project page](https://www.drupal.org/project/cryptolog).


## Credits

Cryptolog module was inspired by the [Cryptolog log filter
script](https://github.com/EFForg/cryptolog), and is maintained by
[mfb](https://www.drupal.org/u/mfb).
