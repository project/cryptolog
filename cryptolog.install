<?php

/**
 * @file
 * Install, update and uninstall functions for the cryptolog module.
 */

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 *
 * @phpstan-ignore missingType.iterableValue
 */
function cryptolog_requirements(string $phase): array {
  $requirements['cryptolog_ipv6']['title'] = t('Cryptolog IPv6 support');
  $requirements['cryptolog_ipv6']['description'] = t('Cryptolog requires that PHP was compiled with IPv6 support enabled.');
  if ((extension_loaded('sockets') && defined('AF_INET6')) || @inet_pton('::1')) {
    $requirements['cryptolog_ipv6']['value'] = t('Enabled');
    $requirements['cryptolog_ipv6']['severity'] = REQUIREMENT_OK;
  }
  else {
    $requirements['cryptolog_ipv6']['value'] = t('Disabled');
    $requirements['cryptolog_ipv6']['severity'] = REQUIREMENT_ERROR;
  }
  if ($phase !== 'runtime') {
    return $requirements;
  }
  $requirements['cryptolog_middleware']['title'] = t('Cryptolog middleware');
  $requirements['cryptolog_middleware']['severity'] = REQUIREMENT_INFO;
  if (\Drupal::getContainer()->initialized('cryptolog.middleware')) {
    $requirements['cryptolog_middleware']['value'] = t('Initialized');
    $requirements['cryptolog_middleware']['description'] = t('Cryptolog middleware is currently active.');
  }
  else {
    $requirements['cryptolog_middleware']['value'] = t('Not initialized');
    $requirements['cryptolog_middleware']['description'] = t('Cryptolog middleware is not currently active (e.g. due to command-line environment).');
  }
  $requirements['cryptolog_sodium']['title'] = t('Cryptolog hash function');
  if (function_exists('sodium_crypto_generichash')) {
    $requirements['cryptolog_sodium']['value'] = t('BLAKE2b');
    $requirements['cryptolog_sodium']['severity'] = REQUIREMENT_OK;
    $requirements['cryptolog_sodium']['description'] = t('Cryptolog is using the Sodium PHP extension to generate BLAKE2b keyed hashes.');
  }
  else {
    $requirements['cryptolog_sodium']['value'] = t('HMAC-MD5');
    $requirements['cryptolog_sodium']['severity'] = REQUIREMENT_INFO;
    $requirements['cryptolog_sodium']['description'] = t('The Sodium PHP extension is not available; Cryptolog is falling back to the Hash PHP extension (HMAC-MD5).');
  }
  $floodConfig = \Drupal::config('user.flood');
  $floodWindow = $floodConfig->get('uid_only') ? $floodConfig->get('ip_window') : $floodConfig->get('user_window');
  if (\Drupal::service('cryptolog.middleware')->getTtl() < $floodWindow) {
    $requirements['cryptolog_ttl']['title'] = t('Cryptolog salt TTL');
    $requirements['cryptolog_ttl']['value'] = \Drupal::service('date.formatter')->formatInterval(\Drupal::service('cryptolog.middleware')->getTtl());
    $requirements['cryptolog_ttl']['severity'] = REQUIREMENT_WARNING;
    $requirements['cryptolog_ttl']['description'] = t('The Cryptolog salt time to live (TTL) is shorter than the login flood control window. Flood control will operate more effectively if the Cryptolog salt TTL is <a href=":url">increased</a>.', [':url' => Url::fromRoute('cryptolog.settings')->toString()]);
  }
  return $requirements;
}
